﻿#include<stdio.h>
#include<windows.h>
#include<math.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double R, D, L, S, M_PI=3.14;
	int k;
	printf("Введіть довжину радіуса\nR=");
	scanf_s("%lf", &R);
	D = 2 * R; L = 2 * M_PI * R; S = M_PI * R * R;
	printf("Виберіть необхідне значення\n 1 - R\n 2 - D=2R\n 3 - L=2ПR\n 4 - S=ПR*R\n");
	scanf_s("%d", &k);
	switch (k)
  {
	case 1:
		printf("Радіус=%f\nДіаметр=%f\nДовжина кола=%f\nПлоща кола=%f\n", R, D, L, S);
		break;
	case 2:
		printf("Діаметр=%f\nРадіус=%f\nДовжина кола=%f\nПлоща кола=%f\n", D, R, L, S);
		break;
	case 3:
		printf("Довжина кола=%f\nРадіус=%f\nДіаметр=%f\nПлоща кола=%f\n", L, R, D, L);
		break;
	case 4:
		printf("Площа кола=%f\nРадіус=%f\nДіаметр=%f\nДовжина кола=%f\n", S, R, D, L);
		break;
	default: printf("Невірний вибір");
	}
	return 0;
}