﻿#include<stdio.h>
#include<windows.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int month;
	printf("Введіть номер місяця\n");
	scanf_s("%d", &month);
	switch (month)
	{
	case 1: printf("у січні 31 день\n\n");
		break;
	case 2: printf("у лютому 28 днів\n\n");
		break;
	case 3: printf("у березні 31 день\n\n");
		break;
	case 4: printf("у квітні 30 днів\n\n");
		break;
	case 5: printf("у травні 31 день\n\n");
		break;
	case 6: printf("у червні 30 днів\n\n");
		break;
	case 7: printf("у липні 31 день\n\n");
		break;
	case 8: printf("у серпні 31 день\n\n");
		break;
	case 9: printf("у вересні 30 днів\n\n");
		break;
	case 10: printf("у жовтні 31 день\n\n");
		break;
	case 11: printf("у листопаді 30 днів\n\n");
		break;
	case 12: printf("у грудні 31 день\n\n");
		break;
	default: printf("Число має бути у діапазоні від 1 до 12\n\n");
	}
			return 0;
		
}
